![logo](./resources/logo.png)

## Carcer

A mechanism for projection

### Getting Started

1.  `git clone https://github.com/DreamsMakeToys/carcer.git`
2.  `cd ./carcer`
3.  `yarn install`
4.  `yarn build`
5.  `yarn start`

### Goals

* **_generalize interaction_**
* **_centralize complexity_**

### Forces

* **_progress not perfection_**
* **_javascript is king, and like a good king it should delegate werk that it isn't suited for_**
* **_pretend your users won't misuse_**
